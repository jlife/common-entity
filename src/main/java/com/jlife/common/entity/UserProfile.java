package com.jlife.common.entity;

/**
 * Class to store details information about user in concrete project.
 * Expected that this project will be have the same id field as user.
 *
 * @author Dzmitry Misiuk
 */
public abstract class UserProfile extends Entity {
}
