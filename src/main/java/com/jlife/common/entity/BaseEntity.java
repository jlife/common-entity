package com.jlife.common.entity;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Base Entity. Includes only id field.
 *
 * @author Dzmitry Misiuk
 */
public abstract class BaseEntity implements Serializable{

    @Id
    private String id;

    protected BaseEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
